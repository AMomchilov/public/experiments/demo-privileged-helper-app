//
//  PrivilegedHelperConstants.swift
//  Demo Privileged Helper App
//
//  Created by Alexander Momchilov on 2021-10-26.
//

import XPC

enum PrivilegedHelperConstants {
	/// The domain of the privildged helper, which is also the name of its executable, and its bundle identifier.
	static let domain = "ca.momchilov.Demo-Privileged-Helper-App.helper"
}
