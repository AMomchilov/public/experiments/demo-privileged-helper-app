//
//  PrivilegedHelperProtocol.swift
//  Demo Privileged Helper App
//
//  Created by Alexander Momchilov on 2021-10-26.
//

import XPC

@objc protocol PrivilegedHelperProtocol {
	func getHelperVersion(_ reply: (String) -> Void)
}
