//
//  main.swift
//  DemoPrivilegedHelper
//
//  Created by Alexander Momchilov on 2021-10-23.
//

import Foundation

NSLog("Hello, world! PID \(getpid()) running as uid: \(getuid()), euid: \(geteuid())")

let listener = NSXPCListener(machServiceName: PrivilegedHelperConstants.domain)

class PrivilegedHelperService: PrivilegedHelperProtocol {
	func getHelperVersion(_ reply: (String) -> Void) {
		reply(Bundle.main.object(forInfoDictionaryKey: kCFBundleVersionKey as String) as! String)
	}
}

class C: NSObject, NSXPCListenerDelegate {
	func listener(_ listener: NSXPCListener, shouldAcceptNewConnection newConnection: NSXPCConnection) -> Bool {
	 newConnection.exportedInterface = NSXPCInterface(with: PrivilegedHelperProtocol.self)
	 newConnection.exportedObject = PrivilegedHelperService()
	 newConnection.resume()

	 return true
 }
}
