//
//  ViewController.swift
//  Demo Privileged Helper App
//
//  Created by Alexander Momchilov on 2021-10-23.
//

import Cocoa

class ViewController: NSViewController {

	private var privHelperManager = PrivilegedHelperManager()

	@IBOutlet weak var versionLabel: NSTextField!

	override func viewDidLoad() {
		super.viewDidLoad()

		// Do any additional setup after loading the view.
	}

	override var representedObject: Any? {
		didSet {
		// Update the view, if already loaded.
		}
	}


	@IBAction func installHelper(_ sender: NSButton) {
		do {
			try privHelperManager.install()
		} catch {
			print(error.localizedDescription)
			dump(error)
		}
	}
}

