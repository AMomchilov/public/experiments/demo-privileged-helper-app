//
//  PrivilegedHelperManager.swift
//  PrivilegedHelperDemo
//
//  Created by Alexander Momchilov on 2021-10-22.
//

import Foundation
import ServiceManagement

func copyIntoCString(_ str: UnsafePointer<CChar>) -> UnsafePointer<CChar> {
	let length = strlen(str)
	let result = UnsafeMutablePointer<CChar>.allocate(capacity: length+1)
	result.assign(from: str, count: length+1)
	return UnsafePointer(result)
}

struct PrivilegedHelperManager {
	let kSMRightBlessPrivilegedHelper_pointer = copyIntoCString(kSMRightBlessPrivilegedHelper)

	enum InstallationOutcome {
		case successfullyInstalledHelper
		case failedToInstallHelper
	}

	private var authRef: AuthorizationRef! = nil

	private mutating func getAuthRef() throws {
		let returnCode = AuthorizationCreate(
			nil,
			nil /* equivalent to kAuthorizationEmptyEnvironment */,
			[],
			&self.authRef
		)

		if returnCode != errAuthorizationSuccess {
			// This should never fail.
			throw NSError(domain: NSOSStatusErrorDomain, code: Int(returnCode))
		}
	}

	@discardableResult
	public mutating func install() throws -> InstallationOutcome {
		try self.getAuthRef()

		var authItem = AuthorizationItem(
			name: kSMRightBlessPrivilegedHelper_pointer,
			valueLength: 0,
			value: nil,
			flags: 0
		)

		try withUnsafeMutablePointer(to: &authItem) { authItemP in
			var authRights = AuthorizationRights(count: 1, items: authItemP)

			try withUnsafeMutablePointer(to: &authRights) { authRightsP in
				let result: OSStatus = AuthorizationCopyRights(
					authRef,
					authRightsP,
					nil /* equivalent to kAuthorizationEmptyEnvironment */,
					[.interactionAllowed, .preAuthorize, .extendRights],
					nil
				)

				switch result {
				case errAuthorizationSuccess:
					break
				case errAuthorizationCanceled:
					print("The user cancelled the authorization request")
					throw NSError.init(domain: NSOSStatusErrorDomain, code: Int(result))
				default:
					throw NSError.init(domain: NSOSStatusErrorDomain, code: Int(result))
				}
			}

		}
		// errAuthorizationSuccess

		var unmanagedError: Unmanaged<CFError>?
		
		let helperWasInstalled = SMJobBless(
			kSMDomainSystemLaunchd,
			"ca.momchilov.Demo-Privileged-Helper-App.helper" as CFString,
			authRef,
			&unmanagedError
		)
		
		if let error = unmanagedError?.takeUnretainedValue() {
			defer { unmanagedError?.release() }
			throw error as Error as NSError
		}
		
		return helperWasInstalled
			? .successfullyInstalledHelper
			: .failedToInstallHelper
	}
	
	public func uninstall() {
		
	}
}
